<?php

/**
 * @file
 *
 * An array of preprocessors to fill variables for templates and helper
 * functions to make theming easier.
 */

/**
 * Implements hook_theme().
 */
function moduleassign_theme() {

  return array(
    'moduleassign_modules_fieldset' => array(
      'render element' => 'form',
    ),
  );
}

/**
 * Returns HTML for the modules form.
 *
 * @param $variables
 *   An associative array containing:
 *   - form: A render element representing the form.
 *
 * @ingroup themeable
 */
function theme_moduleassign_modules_fieldset($variables) {

  // NOTE: Function copied and altered from modules/system/system.admin.inc, theme_system_modules_fieldset

  $form = $variables['form'];

  // Individual table headers.
  $rows = array();
  // Iterate through all the modules, which are
  // children of this fieldset.
  foreach (element_children($form) as $key) {
    // Stick it into $module for easier accessing.
    $module = $form[$key];
    $row = array();
    unset($module['assign']['#title']);
    unset($module['enable']['#title']);
    if ($module['#isconfig'])
      $row[] = array('class' => array('checkbox'), 'data' => drupal_render($module['assign']));
    $row[] = array('class' => array('checkbox'), 'data' => drupal_render($module['enable']));
    $label = '<label';
    if (isset($module['enable']['#id'])) {
      $label .= ' for="' . $module['enable']['#id'] . '"';
    }
    $row[] = $label . '><strong>' . drupal_render($module['name']) . '</strong></label>';
    $row[] = drupal_render($module['version']);
    // Add the description, along with any modules it requires.
    $description = drupal_render($module['description']);
    if ($module['#requires']) {
      $description .= '<div class="admin-requirements">' . t('Requires: !module-list', array('!module-list' => implode(', ', $module['#requires']))) . '</div>';
    }
    if ($module['#required_by']) {
      $description .= '<div class="admin-requirements">' . t('Required by: !module-list', array('!module-list' => implode(', ', $module['#required_by']))) . '</div>';
    }
    $row[] = array('data' => $description, 'class' => array('description'));
    // Display links (such as help or permissions) in their own columns.
    if (!$module['#isconfig']) {
      foreach (array('help', 'permissions', 'configure') as $key) {
        $row[] = array('data' => drupal_render($module['links'][$key]), 'class' => array($key));
      }
    }
    unset($module['#isconfig']);
    $rows[] = $row;
  }

  return theme('table', array('header' => $form['#header'], 'rows' => $rows));
}

// EOF
